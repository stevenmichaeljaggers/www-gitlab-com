---
layout: handbook-page-toc
title: "APM Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common links

* Slack channel: [#g_monitor_apm](https://gitlab.slack.com/archives/g_monitor_apm)
* Slack alias: @monitor-apm-group
* Google groups: monitor-apm-group@gitlab.com (whole team), monitor-apm-be@gitlab.com (backend team), and monitor-apm-fe@gitlab.com (frontend team)

## Backend Team members

<%= direct_team(manager_role: 'Engineering Manager, Monitor:APM') %>

## Frontend Team members

<%= direct_team(manager_role: 'Interim Frontend Engineering Manager, Monitor:APM') %>

## Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] Monitor/, direct_manager_role: 'Engineering Manager, Monitor:Health', other_manager_roles: ['Engineering Manager, Monitor:APM', 'Frontend Engineering Manager, Monitor:Health', 'Interim Frontend Engineering Manager, Monitor:APM']) %>

## Responsibilities
{: #monitoring}

The APM group is responsible for:
* Providing the tools required to enable monitoring of GitLab.com
* Packaging these tools to enable all customers to manage their instances easily and completely
* Building integrated monitoring solutions for customers apps into GitLab, including: metrics, logging, and tracing

This team maps to the [APM Group](/handbook/product/categories/#apm-group) category.

## How to work with APM

### Adding new metrics to GitLab

The APM Group is responsible for providing the underlying libraries and tools to enable GitLab team-members to instrument their code. When adding new metrics, we need to consider a few facets: the impact on GitLab.com, customer deployments, and whether any default alerting rules should be provided.

Recommended process for adding new metrics:

1. Open an issue in the desired project outlining the new metrics desired
1. Label with the ~group::apm label, and ping @gl-monitoring for initial review
1. During implementation consider:
   1. The Prometheus [naming](https://prometheus.io/docs/practices/naming/) and [instrumentation](https://prometheus.io/docs/practices/instrumentation/) guidelines
   1. Impact on cardinality and performance of Prometheus
   1. Whether any alerts should be created
1. Assign to an available APM Group reviewer

## How We Work

We try to adhere to best practices from across the company in how we work. For example, our Product Manager owns the problem validation backlog and problem validation process as outlined in the [Product Development Workflow](https://about.gitlab.com/handbook/product-development-flow/) and follows the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). Engineers follow the [Engineering Workflow](https://about.gitlab.com/handbook/engineering/workflow/).

In addition, here are some additional details on how we work.

### Adding New Issues

When adding a new issue for the Monitor:APM group, follow these guidelines:

* Add the `group::apm` and `devops::monitor` label
* Do not add a specific milestone. New issues will be reviewed and scheduled appropriately.
* For bugs, include [priority and severity labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels). These may be updated, but it is helpful to understand the expectation.

When creating a new issue, try to consider if this issue can be completed in a single milestone, with the collaboration of at most one frontend and/or one backend engineers and one UX team member. If your issue is larger than that, consider creating an epic or splitting your issue in smaller issues.

On a regular basis the product manager will review any new issues and schedule them for the correct milestone. This often happens during the Monitor:APM weekly meeting.

### Prioritizing Issues

Before the start of a milestone, the product manager is responsible for organizing the [APM Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1065731) by putting all issues for the upcoming milestone in priority order. By using the planning board as a priority list, and by keeping it in order, then we should always be able to look at the current and upcoming milestone columns to have a prioritized list of upcoming work.

### Starting a Milestone

To start the next milestone, the engineering manager will apply the [`deliverable`](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#release-scoping-labels) label to any issues that we have a high likelyhood of completing.

The product manager will apply the `release post item` label to the top issues for the upcoming milestone that we want to highlight in the Kickoff call.

### Assigning Issues

As an engineer is available to start a new issue, he/she can self-assign the next highest priority issue. Once assigned, the engineer is responsible for keeping the workflow labels up-to-date and providing async issue updates (see below). If the issue will not be complete in the current milestone, the engineer assigned is also responsible for rescheduling the issue.

### Workflow Labels

We use standard workflow labels on issues as described in the [product development flow](https://about.gitlab.com/handbook/product-development-flow/#build-phase-2-develop--test). Specifically we use `workflow::ready for development` when the issue has enough information to start development, `workflow::In dev` as we are working on the issue, `workflow::In review` when a merge request is in review, and `workflow::verification` after the merge request has been merged and we are testing the change in staging and production.

It is the responsibility of the assigned engineer for an issue to keep the workflow label up-to-date for the issue. We use the [APM Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1165027) board to visualize the issues.

### Async Issue Updates

Accross the [Monitor Stage](/handbook/engineering/development/ops/monitor/) we add an update to each issue that is in progress once a week. See the [Weekly Async issue updates](/handbook/engineering/development/ops/monitor/#weekly-async-issue-updates) section for more details.

### Rescheduling Issues

Towards the end of a milestone, if we find any issues that are not going to be completed, it is the responsibility of the assigned engineer to follow this process for moving the issue to the next milestone.

1. Add a comment to the issue with what work is remaining.
1. Add the `to schedule` label
1. Add an issue weight (see below)
1. Move to the next milestone

#### Issue Weights

We only use issue weights when we have to move an issue from one milestone to the next. This is to help us understand how much remaining work we have for any issue that had to move. For example, we may schedule an issue that just needs a final review differently than an issue that has not been started. We use a simple 1 to 10 scale to estimate the remaining work:

| Weight | Meaning       |
| ------ | ------------- |
| 1      | 10% Remaining |
| 5      | 50% Remaining |
| 10     | 100% Remaining/Not Started |

## Recurring Meetings
While we try to keep our process pretty light on meetings, we do hold a [Monitor APM Weekly Meeting](https://docs.google.com/document/d/1Y9woIjy7ySV3lbIJHuoyROYPZhtO1L_w3XDhgGKzZt8/edit?usp=sharing) to triage and prioritize new issues, discuss our upcoming issues, and uncover any unknowns.

## Async Daily Standups
The purpose of our async standups is to allow every team member to have insight into what everyone else is doing and whether anyone is blocked and could use help. This should not be an exhaustive list of all of your tasks for the day, but rather a summary of the major deliverable you are hoping to achieve. All question prompts are optional. We use the [geekbot slack plugin](https://geekbot.io/) to automate our async standup in the [#g_monitor_standup_apm](https://gitlab.slack.com/messages/CNYUY47FW) channel. Every team member should be added to the async standup by their manager.

## Repos we own or use
* [Prometheus Ruby Mmap Client](https://gitlab.com/gitlab-org/prometheus-client-mmap) - The ruby Prometheus instrumentation lib we built, which we used to instrument GitLab.
* [GitLab](https://gitlab.com/gitlab-org/gitlab) - Where much of the user facing code lives.
* [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab) and [Charts](https://gitlab.com/charts/charts.gitlab.io) - Where the packaging related work goes on as we ship GitLab fully instrumented with a Prometheus instance.

## Issue boards

* [APM](https://gitlab.com/groups/gitlab-org/-/boards/1143499) - Main board with all issues labeled "group::apm"
* [APM - Planning](https://gitlab.com/groups/gitlab-org/-/boards/1065731) - APM issues organized by milestone
* [APM - Product](https://gitlab.com/groups/gitlab-org/-/boards/1117038) - APM issues organized by issue type label like "feature", "bug", "backstage", "security", or "Community contribution"
* [APM - Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1165027) - APM issues organized by workflow label of "ready for development", "in dev", or "in review".
